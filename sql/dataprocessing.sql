DROP TABLE IF EXISTS word_count;
create table word_count (
id serial primary key,
word varchar(50) not null,
count bigint not null
);

DROP TABLE IF EXISTS word_count_3000;
create table word_count_3000 (
id serial primary key,
word varchar(50) not null,
count bigint not null
);

GRANT ALL PRIVILEGES ON TABLE word_count TO root;

GRANT ALL PRIVILEGES ON TABLE word_count_3000 TO root;

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO root;

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO root