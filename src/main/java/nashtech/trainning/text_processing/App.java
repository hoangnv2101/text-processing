package nashtech.trainning.text_processing;

import java.util.Scanner;

import nashtech.trainning.text_processing.cons.MenuOptionsType;

public class App {

    public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);
	boolean isExit = false;
	int choice = 0;
	DataProcessingFactory factory = new DataProcessingFactory();
	DataProcessing processing = null;
	do {
	    do {
		System.out.println("********************************");
		System.out.println("1.Word count");
		System.out.println("2.Word count and sorted by alphabet");
		System.out.println("3.Word count by parallel (per file)");
		System.out.println("4.Insert into table word count");
		System.out.println("5.Insert into table word count 3000");
		System.out
			.println("6.Insert into table word count 3000 with priority order");
		System.out
			.println("7.Find and count email and export to one result file");
		System.out.println("8.Exit");
		System.out.println("********************************");

		System.out.println("Please enter your choice :  ");

		while (!scanner.hasNextInt()) {
		    System.out
			    .println("That's not a number! Please choose again!");
		    scanner.next();
		}

		choice = scanner.nextInt();

		switch (choice) {
		case 1:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT);
		    break;
		case 2:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_SORT);
		    break;
		case 3:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_PARAREL);
		    break;
		case 4:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_INSERT_DB);
		    break;
		case 5:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_3000_INSERT_DB);
		    break;
		case 6:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_3000_INSERT_DB_PRIORITY);
		    break;
		case 7:
		    processing = factory
			    .processData(MenuOptionsType.WORD_COUNT_EMAIL);
		    break;
		default:
		    System.out.println("Invalid Option");
		    choice = 0;
		    break;
		}
	    } while (choice <= 0);

	    if (!isExit && processing != null) {

		System.out.println("Number of CPU cores "
			+ Runtime.getRuntime().availableProcessors());

		Runtime runtime = Runtime.getRuntime();
		runtime.gc();

		long usedMemoryBefore = runtime.totalMemory()
			- runtime.freeMemory();

		System.out.println("Used Memory before in byte "
			+ usedMemoryBefore);

		long startTime = System.nanoTime();

		processing.process();

		long endTime = System.nanoTime();
		double duration = (endTime - startTime);

		runtime.gc();

		long usedMemoryAfter = runtime.totalMemory()
			- runtime.freeMemory();

		System.out.println("Used Memory after in byte "
			+ usedMemoryAfter);

		System.out.println("Used memory is bytes: "
			+ (usedMemoryAfter - usedMemoryBefore));

		System.out.println("That took " + duration
			+ " nano miniseconds");
	    }

	} while (!isExit);
	scanner.close();
    }
}
