package nashtech.trainning.text_processing.impl;

import java.util.Map;

import nashtech.trainning.text_processing.DataProcessing;
import nashtech.trainning.text_processing.cons.Constant;
import nashtech.trainning.text_processing.utils.DataProcessingHelper;
import nashtech.trainning.text_processing.utils.PropertiesUtil;

public class WordCountEmail implements DataProcessing {

    @Override
    public void process() {
	// TODO Auto-generated method stub
	Map<String, Integer> wordCount = DataProcessingHelper
		.getWordsEmailCountDirectory(
			PropertiesUtil.getPropValues(Constant.WORD_FOLDER));

	DataProcessingHelper.writeFile(
		PropertiesUtil.getPropValues(Constant.FILE_PATH), wordCount);

    }

}
