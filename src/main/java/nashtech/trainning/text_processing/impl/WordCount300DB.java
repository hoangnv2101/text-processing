package nashtech.trainning.text_processing.impl;

import java.util.Map;

import nashtech.trainning.text_processing.DataProcessing;
import nashtech.trainning.text_processing.cons.Constant;
import nashtech.trainning.text_processing.repository.WordCountRepository;
import nashtech.trainning.text_processing.utils.DataProcessingHelper;
import nashtech.trainning.text_processing.utils.PropertiesUtil;

public class WordCount300DB implements DataProcessing {

    @Override
    public void process() {

	Map<String, Integer> wordCount = DataProcessingHelper
		.getWordsCountByDirectory(
			PropertiesUtil.getPropValues(Constant.WORD_FOLDER),
			false, true);

	boolean isDesc = Boolean.parseBoolean(PropertiesUtil
		.getPropValues(Constant.IS_DESC));

	wordCount = DataProcessingHelper.sortByKey(wordCount, isDesc);

	WordCountRepository repository = new WordCountRepository();

	repository.saveToWordCount3000(wordCount);
    }
}