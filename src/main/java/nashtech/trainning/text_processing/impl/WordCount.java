package nashtech.trainning.text_processing.impl;

import java.util.Map;

import nashtech.trainning.text_processing.DataProcessing;
import nashtech.trainning.text_processing.cons.Constant;
import nashtech.trainning.text_processing.utils.DataProcessingHelper;
import nashtech.trainning.text_processing.utils.PropertiesUtil;

public class WordCount implements DataProcessing {

    @Override
    public void process() {

	Map<String, Integer> wordCount = DataProcessingHelper
		.getWordsCountByDirectory(
			PropertiesUtil.getPropValues(Constant.WORD_FOLDER),
			false, false);
	DataProcessingHelper.writeFile(
		PropertiesUtil.getPropValues(Constant.FILE_PATH), wordCount);
    }
}
