package nashtech.trainning.text_processing.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import nashtech.trainning.text_processing.cons.Constant;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PropertiesUtil {
    private static Log log = LogFactory.getLog(PropertiesUtil.class);

    public static String getPropValues(String name) {
	Properties prop = new Properties();
	InputStream input = null;

	try {
	    input = new FileInputStream(Constant.PROPERTIES_PATH);

	    // load the properties file
	    prop.load(input);

	} catch (IOException ex) {
	    log.error("PropertiesUtil file not found !!", ex);
	} finally {
	    if (input != null) {
		try {
		    input.close();
		} catch (IOException e) {
		    log.error("InputStream not found !!", e);
		}
	    }
	}
	return prop.getProperty(name);
    }
}
