package nashtech.trainning.text_processing.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListFilesUtil {

    public static List<File> listFiles(String directoryName) {
	List<File> result = new ArrayList<>();

	// get all the files from a directory
	File[] fList = new File(directoryName).listFiles();
	for (File file : fList) {
	    if (file.isFile()) {
		result.add(file);
	    }
	}
	return result;
    }
}
