package nashtech.trainning.text_processing.utils;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DbUtil {
    private static Connection dbConnection = null;
    private static Log log = LogFactory.getLog(DbUtil.class);

    public static Connection getConnection() {
	if (dbConnection != null) {
	    return dbConnection;
	} else {
	    try {
		String dbDriver = PropertiesUtil.getPropValues("dbDriver");
		String connectionUrl = PropertiesUtil
			.getPropValues("connectionUrl");
		String userName = PropertiesUtil.getPropValues("userName");
		String password = PropertiesUtil.getPropValues("password");

		Class.forName(dbDriver).newInstance();
		dbConnection = DriverManager.getConnection(connectionUrl,
			userName, password);
	    } catch (Exception ex) {
		log.error("DbUtil connection error ", ex);
	    }

	    return dbConnection;
	}
    }
}
