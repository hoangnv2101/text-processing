package nashtech.trainning.text_processing.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nashtech.trainning.text_processing.cons.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DataProcessingHelper {

    private static Log log = LogFactory.getLog(DataProcessingHelper.class);
    private static Map<String, Integer> word3000 = new HashMap<String, Integer>();

    static {
	word3000 = getWordsCountByDirectory(
		PropertiesUtil.getPropValues(Constant.WORD_3000_FOLDER), false,
		false);
    }

    public static <K> Map<K, Integer> mergeAndAdd(List<Map<K, Integer>> maps) {
	Map<K, Integer> result = new HashMap<>();
	for (Map<K, Integer> map : maps) {
	    for (Map.Entry<K, Integer> entry : map.entrySet()) {
		K key = entry.getKey();
		Integer current = result.get(key);
		result.put(key,
			current == null ? entry.getValue() : entry.getValue()
				+ current);
	    }
	}
	return result;
    }

    public static Map<String, Integer> getWordsCountByFile(File file,
	    boolean isFilterByWord300) {

	FileInputStream inputStream = null;
	Scanner sc = null;

	HashMap<String, Integer> wordCounts = new HashMap<String, Integer>();
	try {
	    inputStream = new FileInputStream(file.getPath());
	    sc = new Scanner(inputStream);
	    while (sc.hasNextLine()) {
		String line = sc.nextLine();
		buildWordCount(isFilterByWord300, wordCounts, line);
	    }

	} catch (Exception e) {
	    log.error("getWordsCountByFile", e);

	} finally {
	    if (inputStream != null) {
		try {
		    inputStream.close();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    log.error("getWordsCountByFile IOException", e);
		}
	    }
	    if (sc != null) {
		sc.close();
	    }
	}
	return wordCounts;
    }

    public static Map<String, Integer> getWordsCountEmailByFileParalalel(File file) {

	Map<String, Integer> wordCounts = new HashMap<String, Integer>();
	FileInputStream inputStream = null;
	Scanner sc = null;

	try {
	    
	    inputStream = new FileInputStream(file.getPath());
	    sc = new Scanner(inputStream);
	    
	    ExecutorService executor = Executors.newFixedThreadPool(Runtime
		    .getRuntime().availableProcessors() - 1);

	    List<Callable<Map<String, Integer>>> callables = new ArrayList<Callable<Map<String, Integer>>>();

	    while (sc.hasNextLine()) {
		String line = sc.nextLine();
		
		Callable<Map<String, Integer>> task = () -> {
		    Map<String, Integer> map = new HashMap<String, Integer>();

		    Stream.of(line).map(l -> l.split("\\s+"))
			    .flatMap(Arrays::stream)
			    .filter(w -> w.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+")).map(x -> {
				return StringUtils.capitalize(x.toLowerCase());
			    }).forEach(word -> {
				    if (!map.containsKey(word)) {
					map.put(word, 1);
				    } else {
					map.put(word, map.get(word) + 1);
				}
			    });
		    return map;
		};
		callables.add(task);
	    };
	    
	    List<Map<String, Integer>> result = executor.invokeAll(callables)
		    .stream().map(future -> {
			try {
			    return future.get();
			} catch (Exception e) {
			    throw new IllegalStateException(e);
			}
		    }).collect(Collectors.toList());

	    executor.shutdown();
	    
	    wordCounts = mergeAndAdd(result);

	} catch (Exception e) {
	    log.error("getWordsCountEmailByFile", e);

	} finally {
	    if (inputStream != null) {
		try {
		    inputStream.close();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    log.error("getWordsCountByFile IOException", e);
		}
	    }
	    if (sc != null) {
		sc.close();
	    }
	}
	return wordCounts;
    }
    
    public static Map<String, Integer> getWordsCountByFileParalalel(File file,
	    boolean isFilterByWord300) {

	Map<String, Integer> wordCounts = new HashMap<String, Integer>();
	FileInputStream inputStream = null;
	Scanner sc = null;

	try {
	    inputStream = new FileInputStream(file.getPath());
	    sc = new Scanner(inputStream);
	    
	    ExecutorService executor = Executors.newFixedThreadPool(Runtime
		    .getRuntime().availableProcessors() - 1);

	    List<Callable<Map<String, Integer>>> callables = new ArrayList<Callable<Map<String, Integer>>>();

	    while (sc.hasNextLine()) {
		String line = sc.nextLine();
		
		Callable<Map<String, Integer>> task = () -> {
		  
		    Map<String, Integer> map = new ConcurrentHashMap<String, Integer>();
		    buildWordCount(isFilterByWord300, map, line);
		    return map;
		};
		callables.add(task);
	    };
	    
	    List<Map<String, Integer>> result = executor.invokeAll(callables)
		    .stream().map(future -> {
			try {
			    return future.get();
			} catch (Exception e) {
			    throw new IllegalStateException(e);
			}
		    }).collect(Collectors.toList());

	    executor.shutdown();
	    wordCounts = mergeAndAdd(result);

	} catch (Exception e) {
	    log.error("getWordsCountByFile", e);

	} finally {
	    if (inputStream != null) {
		try {
		    inputStream.close();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    log.error("getWordsCountByFile IOException", e);
		}
	    }
	    if (sc != null) {
		sc.close();
	    }
	}
	return wordCounts;
    }

    public static Map<String, Integer> getWordsCountByDirectory(
	    String directory, boolean isReadParallel, boolean isFilterByWord300) {
	List<Map<String, Integer>> listOfMaps = new ArrayList<Map<String, Integer>>();

	List<File> files = ListFilesUtil.listFiles(directory);

	for (File file : files) {

	    if (isReadParallel) {
		listOfMaps.add(getWordsCountByFileParalalel(file,
			isFilterByWord300));
	    } else {
		listOfMaps.add(getWordsCountByFile(file, isFilterByWord300));
	    }
	}

	Map<String, Integer> wordCount = mergeAndAdd(listOfMaps);

	return wordCount;
    }
    
    public static Map<String, Integer> getWordsEmailCountDirectory(
	    String directory) {
	List<Map<String, Integer>> listOfMaps = new ArrayList<Map<String, Integer>>();

	List<File> files = ListFilesUtil.listFiles(directory);

	for (File file : files) {

	    listOfMaps.add(getWordsCountEmailByFileParalalel(file));
	}

	Map<String, Integer> wordCount = mergeAndAdd(listOfMaps);

	return wordCount;
    }

    public static void writeFile(String fileName, Map<String, Integer> maps) {
	FileOutputStream fos;
	try {
	    fos = new FileOutputStream(fileName);

	    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	    maps.forEach((key, value) -> {
		String line = key + ":" + value;
		try {
		    bw.write(line);
		    bw.newLine();
		} catch (Exception e) {
		    System.err.println(e.getMessage());
		}
	    });
	    bw.close();
	} catch (Exception e) {
	    log.error("DataProcessingUtil writeFile Exception", e);
	}
    }

    public static Map<String, Integer> sortByKey(
	    Map<String, Integer> unsortMap, boolean isDesc) {
	Map<String, Integer> sortedMap = new TreeMap<>((Comparator<String>) (
		o1, o2) -> {
	    if (isDesc) {
		return o1.compareTo(o2);
	    } else {
		return o2.compareTo(o1);
	    }
	});

	sortedMap.putAll(unsortMap);

	return sortedMap;
    }

    public static Map<String, Integer> sortByKeyAndLenght(
	    Map<String, Integer> unsortMap, boolean isDesc) {
	Map<String, Integer> sortedMap = new TreeMap<>((Comparator<String>) (
		o1, o2) -> {
	    if (isDesc) {

		if (o1.length() > o2.length()) {
		    return 1;
		}
		if (o1.length() < o2.length()) {
		    return -1;
		} else {
		    return o1.compareTo(o2);
		}
	    } else {
		if (o2.length() > o1.length()) {
		    return 1;
		}
		if (o2.length() < o1.length()) {
		    return -1;
		} else {
		    return o2.compareTo(o1);
		}
	    }
	});

	sortedMap.putAll(unsortMap);

	return sortedMap;
    }
    
    private static void buildWordCount(boolean isFilterByWord300,
	    Map<String, Integer> wordCounts, String line) {
	Stream.of(line)
		.map(l -> l.split("\\s+"))
		.flatMap(Arrays::stream)
		.filter(w -> w.matches("\\w+"))
		.map(x -> {
		    return StringUtils.capitalize(x.toLowerCase());
		})
		.forEach(
			word -> {
			    if (isFilterByWord300) {
				if (word3000.containsKey(word)) {
				    if (!wordCounts.containsKey(word)) {
					wordCounts.put(word, 1);
				    } else {
					wordCounts.put(
						word,
						wordCounts.get(word) + 1);
				    }
				}
			    } else {
				if (!wordCounts.containsKey(word)) {
				    wordCounts.put(word, 1);
				} else {
				    wordCounts.put(word,
					    wordCounts.get(word) + 1);
				}
			    }
			});
    }
}
