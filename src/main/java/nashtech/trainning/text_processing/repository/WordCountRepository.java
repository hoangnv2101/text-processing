package nashtech.trainning.text_processing.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import nashtech.trainning.text_processing.utils.DbUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class WordCountRepository {
    private Connection dbConnection;
    private static Log log = LogFactory.getLog(WordCountRepository.class);

    public WordCountRepository() {
	dbConnection = DbUtil.getConnection();
    }

    public void saveToWordCount(Map<String, Integer> words) {
	String sql = "insert into word_count(word, count) values (?, ?)";
	this.save(words, sql);
    }

    public void saveToWordCount3000(Map<String, Integer> words) {
	String sql = "insert into word_count_3000(word, count) values (?, ?)";
	this.save(words, sql);
    }

    private void save(Map<String, Integer> words, String sql) {
	PreparedStatement prepStatement = null;
	try {

	    if (dbConnection != null) {
		prepStatement = dbConnection.prepareStatement(sql);

		dbConnection.setAutoCommit(false);

		Iterator<Map.Entry<String, Integer>> iterator = words
			.entrySet().iterator();
		int i = 0;
		while (iterator.hasNext()) {
		    Map.Entry<String, Integer> entry = iterator.next();

		    prepStatement.setString(1, entry.getKey());
		    prepStatement.setInt(2, entry.getValue());
		    prepStatement.addBatch();
		    i++;

		    if (i == 1000 || (i < 1000 && !iterator.hasNext())) {
			prepStatement.executeBatch();
			dbConnection.commit();
			prepStatement.clearBatch();
			i = 0;
		    }
		}
		;

	    }

	} catch (ParseException ex) {
	    log.error("ParseException " + ex);
	} catch (SQLException ex) {
	    log.error("SQLException " + ex);
	    if (dbConnection != null) {
		try {
		    System.out.println("Transaction is being rolled back.");
		    dbConnection.rollback();
		} catch (Exception e) {
		    log.error("SQLException cannot roll back " + ex);
		}
	    }
	} finally {
	    try {
		if (prepStatement != null) {
		    prepStatement.close();
		}
	    } catch (SQLException ex) {
		log.error("SQLException " + ex);
	    }
	}
    }
}
