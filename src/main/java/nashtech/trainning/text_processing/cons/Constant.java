package nashtech.trainning.text_processing.cons;

public final class Constant {

    public static final String WORD_FOLDER = "word-count";
    public static final String WORD_3000_FOLDER = "word-count-3000";
    public static final String FILE_PATH = "filePath";
    public static final String IS_DESC = "isDesc";
    public static final String PROPERTIES_PATH = "./config/config.properties";
}
