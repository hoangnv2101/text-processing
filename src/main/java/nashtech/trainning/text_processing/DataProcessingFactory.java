package nashtech.trainning.text_processing;

import nashtech.trainning.text_processing.cons.MenuOptionsType;
import nashtech.trainning.text_processing.impl.WordCount;
import nashtech.trainning.text_processing.impl.WordCount300DB;
import nashtech.trainning.text_processing.impl.WordCountDB;
import nashtech.trainning.text_processing.impl.WordCountEmail;
import nashtech.trainning.text_processing.impl.WordCountParallel;
import nashtech.trainning.text_processing.impl.WordSortedCount;
import nashtech.trainning.text_processing.impl.WordSortedCount3000;

public class DataProcessingFactory {

    public DataProcessing processData(MenuOptionsType option) {

	switch (option) {
	case WORD_COUNT:
	    return new WordCount();
	case WORD_COUNT_SORT:
	    return new WordSortedCount();
	case WORD_COUNT_PARAREL:
	    return new WordCountParallel();
	case WORD_COUNT_INSERT_DB:
	    return new WordCountDB();
	case WORD_COUNT_3000_INSERT_DB:
	    return new WordCount300DB();
	case WORD_COUNT_3000_INSERT_DB_PRIORITY:
	    return new WordSortedCount3000();
	case WORD_COUNT_EMAIL:
	    return new WordCountEmail();
	default:
	    throw new IllegalArgumentException("Invalid Invocation");
	}
    }
}
